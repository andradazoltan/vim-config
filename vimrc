" Colors {{{
syntax enable " enable syntax processing
if has('termguicolors')
    set termguicolors
endif
set background=dark " set dark background
colorscheme everforest
set t_ut=
" }}}

" Spaces & Tabs {{{
set tabstop=4 " number of visual spaces per TAB
set softtabstop=4 " number of visual spaces per TAB when editing
set expandtab " replace <TAB> key with equivalent spaces
" }}}

" UI Config {{{
filetype indent on " load filetype-specific indent files at indent/*.vim
set number " show line numbers
set showcmd " show last vim command in bottom bar
set wildmenu " visual autocomplete for command menu *very optional*
set lazyredraw " redraw only when needed
set showmatch " highlight matching brackets [{()}]
" }}}

" Searching {{{
set incsearch " search as characters are entered
set hlsearch " highlight matches
" }}}

" Movement {{{
set virtualedit=all " use virtual space

" move vertically by visual line (jump in line wraps)
nnoremap j gj
nnoremap k gk
" }}}

" Leader Shortcuts {{{
let mapleader=";" " leader is ;

" map turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>

" move to the beginning/end of a line
nnoremap <leader>h ^
nnoremap <leader>j j$
nnoremap <leader>k k$
nnoremap <leader>l $

" Draw line at 100 characters
set colorcolumn=100

" save a session (requires <CR> to complete)
nnoremap <leader>s :mksession<CR>
" }}}

" Jump back to prevous location when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" Highlight verilog
autocmd BufNewFile,BufRead *.v,*.vs set syntax=verilog

